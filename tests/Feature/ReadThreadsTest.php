<?php

namespace Tests\Feature;

use App\Channel;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ReadThreadsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->thread = create('App\Thread');
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAUserCanBrowseThreads()
    {
        $response = $this->get('/threads');

        $response->assertSee($this->thread->title);
    }

    /**
     * Test viewing single thread
     */
    public function testAUserCanSeeSingleThread()
    {
        $response = $this->get($this->thread->path());

        $response->assertSee($this->thread->title);
    }

    /**
     * Test filter threads based on channels
     */
    public function testAUserCanFilterThreadsBelongsToChannel()
    {
        // create channel and threads belongs to that channel
        $channel = create('App\Channel');
        $threadInChannel = create('App\Thread', ['channel_id' => $channel->id]);
        // go to route GET /threads/{channel}
        $channelSlug = $channel->slug;
        if ($channelSlug) {
            $this->get("/threads/{$channelSlug}")
                ->assertSee($threadInChannel->title);
        } else {
            $this->testAUserCanBrowseThreads();
        }
    }

    /**
     * A user can filter threads by username
     */
    public function testAUserCanFilterThreadsByAnyUsername()
    {
        $this->signIn();
        // create threads belongs to specific user
        $user = create('App\User');
        $threadBelongsToUser = create('App\Thread', ['user_id' => $user->id]);
        $threadNotBelongsToUser = create('App\Thread');
        // get to endpoint /threads?by=username
        $this->get("threads?by={$user->name}")
            ->assertSee($threadBelongsToUser->title)
            ->assertDontSee($threadNotBelongsToUser->title);
    }

    /**
     * Test sort threads by number of replies
     */
    public function testAUserFilterThreadsByReplies()
    {
        // Given 3 threads - 2 replies, 3 replies, 0 replies
        $threadsWithTwoReplies = create('App\Thread');
        create('App\Reply', ['thread_id' => $threadsWithTwoReplies->id], 2);
        $threadsWithThreeReplies = create('App\Thread');
        create('App\Reply', ['thread_id' => $threadsWithThreeReplies->id], 3);

        $threadWithNoReplies = $this->thread;

        // Filter all threads by popularity
        $response = $this->getJson('/threads?popular=1')->json();

        // Return from most replies to least
        $this->assertEquals([3, 2, 0], array_column($response, 'replies_count'));
    }

    public function test_a_user_can_filter_unanswered_threads()
    {
        $thread = create('App\Thread');

        create('App\Reply', ['thread_id' => $thread->id]);

        $response = $this->getJson('/threads?unanswered')->json();

        $this->assertCount(1, $response);
    }

    public function test_a_user_can_see_replies_for_a_given_thread()
    {
        $this->signIn();

        $thread = create('App\Thread');

        create('App\Reply', ['thread_id' => $thread->id], 2);

        $response = $this->json('GET', $thread->path() . '/replies');

        $this->assertCount(2, $response['data']);

        $this->assertEquals(2, $response['total']);
    }

    public function test_user_can_see_unanswered_threads()
    {
        $thread = create('App\Thread');

        $reply = create('App\Reply', ['thread_id' => $thread->id]);

        $response = $this->getJson('/threads?unanswered=1')->json();

        $this->assertCount(1, $response);
    }
}
