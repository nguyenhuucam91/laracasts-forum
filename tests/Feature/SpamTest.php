<?php

namespace Tests\Feature;

use App\Utils\Inspections\Spam;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SpamTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * Test replies contains spam keywords must be blocked
     *
     * @return void
     */
    public function test_replies_that_contain_spam_may_not_be_created()
    {
        $this->signIn();

        $thread = create('App\Thread');

        $reply = create('App\Reply', [
            'body' => 'Yahoo customer support'
        ]);

        $this->expectException(\Exception::class);

        $this->post($thread->path() . '/replies', $reply->toArray());
    }

    /**
     * Test replies contains spam keywords must be blocked
     *
     * @return void
     */
    public function test_replies_that_not_contain_spam_may_be_created()
    {
        $this->signIn();

        $spam = new Spam();

        $this->assertFalse($spam->spamDetected('s'));
    }

    /**
     * Test replies whether reply's body contains key which number of subsequent chars >= 4
     *
     * @return void
     */
    public function test_replies_contains_key_held_down_is_invalid()
    {
        $this->signIn();

        $spam = new Spam();

        $this->assertTrue($spam->spamDetected('Hello World aaaaaaaaaaaaaa'));
    }

    /**
     * Test replies whether reply's body contains key which number of subsequent chars >= 4
     *
     * @return void
     */
    public function test_replies_not_contains_key_held_down_is_valid()
    {
        $this->signIn();

        $spam = new Spam();

        $this->assertFalse($spam->spamDetected('Hello World aa'));
    }
}
