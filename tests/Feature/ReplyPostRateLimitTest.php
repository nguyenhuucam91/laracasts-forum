<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Redis;
use Tests\TestCase;

class ReplyPostRateLimitTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        Redis::flushdb();
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_create_reply_within_rate_limit()
    {
        $this->signIn();

        $thread = create('App\Thread');

        $replyData = [
            'body' => 'Replies',
            'user_id' => auth()->id(),
        ];

        $this->post($thread->path() . '/replies', $replyData);

        $this->assertDatabaseHas('replies', $replyData);
    }

    /**
     * Create post which bypass rate limit
     *
     * @return void
     */
    public function test_create_replies_can_break_rate_limit()
    {
        $this->signIn();

        $thread = create('App\Thread');

        $replyData = [
            'body' => 'Replies',
            'user_id' => auth()->id(),
        ];

        $this->expectException(\App\Exceptions\ByPassRateLimitingException::class);

        for ($i = 1; $i <= 35; $i++) {
            $this->post($thread->path() . '/replies', $replyData);
        }
    }
}
