<?php

namespace Tests\Feature;

use App\Notifications\MentionUserNotification;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class MentionUsersTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * Test_mention_users_in_a_reply_are_notified
     *
     * @return void
     */
    public function test_mention_single_user_in_a_reply_are_notified()
    {
        Notification::fake();
        // Given signed-in user
        $this->signIn();
        // Create user in the system to mention
        $user = create('App\User', [
            'name' => 'camnh1'
        ]);
        // Given a thread
        $thread = create('App\Thread');
        // When user mention @JaneDoe in a reply
        $this->json("POST", "{$thread->path()}/replies", [
            'body' => '@camnh1 hello'
        ]);
        // Then JaneDoe is notified
        Notification::assertSentTo($user, MentionUserNotification::class);
    }

    /**
     * Test_mention_users_in_a_reply_are_notified
     *
     * @return void
     */
    public function test_mention_users_in_a_reply_are_notified()
    {
        Notification::fake();
        // Given signed-in user
        $this->signIn();
        // Create user in the system to mention
        $camnh1 = create('App\User', [
            'name' => 'camnh1'
        ]);

        $camnh2 = create('App\User', [
            'name' => 'camnh2'
        ]);
        // Given a thread
        $thread = create('App\Thread');
        // When user mention @JaneDoe in a reply
        $this->json("POST", "{$thread->path()}/replies", [
            'body' => '@camnh1 hello @camnh2 with @camnh3'
        ]);
        // Then JaneDoe is notified
        Notification::assertSentTo($camnh1, MentionUserNotification::class);
        Notification::assertSentTo($camnh2, MentionUserNotification::class);
    }
}
