<?php

namespace Tests\Feature;

use App\Utils\Spam;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ParticipateInForumTest extends TestCase
{
    use DatabaseMigrations;

    public function testUnauthenticatedUserMayNotAddReplies()
    {
        $this->withExceptionHandling()
            ->post('/threads/some-channel/1/replies', [])
            ->assertRedirect('/login');
    }

    /**
     * Participate in forum threads
     */
    public function test_reply_require_a_body()
    {
        $this->withExceptionHandling();

        $this->signIn();

        $thread = create('App\Thread');

        $reply = make('App\Reply', ['body' => null]);

        $this->post($thread->path() . '/replies', $reply->toArray())
            ->assertSessionHasErrors('body');
    }

    public function test_unauthorized_users_cannot_delete_replies()
    {
        $this->withExceptionHandling();
        $reply = create('App\Reply');

        $this->delete("/replies/{$reply->id}")
            ->assertRedirect('login');

        $this->signIn()
            ->delete("/replies/{$reply->id}")
            ->assertStatus(403);
    }

    public function test_authorized_user_can_delete_reply()
    {
        $this->signIn();

        $reply = create('App\Reply', ['user_id' => auth()->user()->id]);

        $this->delete("/replies/{$reply->id}");

        $this->assertDatabaseMissing('replies', ['id' => $reply->id]);
    }

    public function test_authorized_user_can_update_reply()
    {
        $this->signIn();

        $reply = create('App\Reply', ['user_id' => auth()->user()->id]);

        $dataForUpdate = [
            'id' => $reply->id,
            'body' => 'Update body test'
        ];

        $this->put("/replies/{$reply->id}", $dataForUpdate);

        $this->assertDatabaseHas('replies', $dataForUpdate);
    }

    public function test_unauthorized_user_cannot_update_reply()
    {
        $this->withExceptionHandling();

        $this->signIn();

        $user = create('App\User');

        $replyNotBelongsToUser = create('App\Reply', ['user_id' => $user->id]);

        $dataForUpdate = [
            'id' => $replyNotBelongsToUser->id,
            'body' => 'Update body test'
        ];

        $response = $this->put("/replies/{$replyNotBelongsToUser->id}", $dataForUpdate);

        $response->assertStatus(403);
    }
}
