<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SubscribeToThreadsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_a_user_can_subscribe_to_thread()
    {
        $this->signIn();

        $thread = create('App\Thread');

        $this->post($thread->path() . '/subscriptions');

        $this->assertCount(1, $thread->fresh()->subscriptions);
    }

    // public function test_

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_a_user_can_unsubscribe_to_thread()
    {
        $this->signIn();

        $thread = create('App\Thread');

        $thread->createSubscriptionBy(auth()->user()->id);

        $this->delete($thread->path() . '/subscriptions');

        $this->assertFalse($thread->isSubscribedTo);
    }
}
