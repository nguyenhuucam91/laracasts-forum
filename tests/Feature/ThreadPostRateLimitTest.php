<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Redis;
use Tests\TestCase;

class ThreadPostRateLimitTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        Redis::flushdb();
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_create_post_within_rate_limit()
    {
        $this->signIn();

        $thread = [
            'title' => 'sample title',
            'body' => 'sample body',
            'channel_id' => create('App\Channel')->id
        ];

        $this->post('/threads', $thread);

        $this->assertDatabaseHas('threads', $thread);
    }

    /**
     * Create post which bypass rate limit
     *
     * @return void
     */
    public function test_create_posts_can_break_rate_limit()
    {
        $this->signIn();

        $thread = [
            'title' => 'sample title',
            'body' => 'sample body',
            'channel_id' => create('App\Channel')->id
        ];

        $this->expectException(\App\Exceptions\ByPassRateLimitingException::class);

        for ($i = 1; $i <= 65; $i++) {
            $this->post('/threads', $thread);
        }
    }
}
