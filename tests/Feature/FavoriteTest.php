<?php

namespace App\Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class FavoritesTest extends TestCase
{
    use DatabaseMigrations;

    public function testGuestCannotFavoriteAnything()
    {
        $this->withExceptionHandling();

        $reply = create('App\Reply');

        $this->post("/replies/{$reply->id}/favorites")->assertRedirect('/login');
    }

    public function testAnAuthenticatedUserCanFavoriteAnyReply()
    {
        $this->signIn();

        $reply = create('App\Reply');
        // if I post to a "favorite" endpoint
        $this->post("/replies/{$reply->id}/favorites");
        // It should be recorded in database
        $this->assertCount(1, $reply->favorites);
    }

    public function testAnAuthenticatedUserCanFavoriteAnyReplyOnlyOnce()
    {
        $this->signIn();

        $reply = create('App\Reply');
        // if I post to a "favorite" endpoint
        $this->post("/replies/{$reply->id}/favorites");
        $this->post("/replies/{$reply->id}/favorites");
        // It should be recorded in database
        $this->assertCount(1, $reply->favorites);
    }

    public function test_authenticated_user_can_unfavorite_reply()
    {
        $this->signIn();

        $reply = create('App\Reply');

        $reply->favorite();

        $this->delete("/replies/{$reply->id}/favorites");

        $this->assertEquals(0, $reply->favoritesCount);
    }
}
