<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAnUserHasProfile()
    {
        $user = create('App\User');

        $response = $this->get("/profiles/{$user->name}");

        $response->assertSee($user->name);
    }

    /**
     *
     */
    public function testProfileDisplayAllThreadsCreatedByAssociatedUser()
    {
        $this->signIn();

        $threadBelongsToUser = create('App\Thread', ['user_id' => auth()->id()]);

        $user = auth()->user();

        $this->get("/profiles/{$user->name}")
            ->assertSee($threadBelongsToUser->title)
            ->assertSee($threadBelongsToUser->body);
    }
}
