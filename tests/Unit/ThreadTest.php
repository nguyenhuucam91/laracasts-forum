<?php

namespace Tests\Unit;

use App\Notifications\ThreadWasUpdated;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class ThreadTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $this->thread = create('App\Thread');
    }

    public function testAThreadCanMakeAStringPath()
    {
        $thread = create('App\Thread');

        $this->assertEquals("/threads/{$thread->channel->slug}/{$thread->id}", $thread->path());
    }

    public function testAThreadHasReplies()
    {
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $this->thread->replies);
    }

    /**
     * Test a thread has creator or not
     */
    public function testAThreadHasCreator()
    {
        $this->assertInstanceOf('App\User', $this->thread->creator);
    }

    /**
     * A thread can add reply
     */
    public function testAThreadCanAddAReply()
    {
        $this->thread->addReply([
            'body' => 'Foobar',
            'user_id' => 1
        ]);

        $this->assertCount(1, $this->thread->replies);
    }

    /**
     * A thread belongs to a channel
     */
    public function testAThreadBelongsToAChannel()
    {
        $thread = create('App\Thread');

        $this->assertInstanceOf('App\Channel', $thread->channel);
    }

    public function test_a_thread_can_be_subscribed_to()
    {
        //Given we have a thread
        $thread = create('App\Thread');
        //And authenticated user
        $this->signIn();
        //When user subscribed to the thread
        $thread->createSubscriptionBy(auth()->user()->id);
        //Then we should be able to fetch all threads that user has subscribed to
        $thread->subscriptions()->where('user_id', auth()->id())->get();

        $this->assertEquals(
            1,
            $thread->subscriptions()->where('user_id', auth()->user()->id)->count()
        );
    }

    public function test_a_thread_can_be_unsubscribed_from()
    {
        $user = $this->signIn();

        $authenticatedUserId = auth()->user()->id;
        // Given we have a thread
        $thread = create('App\Thread');
        // And a user who is subscribed to the thread
        $thread->createSubscriptionBy($authenticatedUserId);
        // When user unsubscribed to thread
        $thread->deleteSubscriptionFrom($authenticatedUserId);

        $this->assertEquals(
            0,
            $thread->subscriptions()->where('user_id', auth()->user()->id)->count()
        );
    }

    public function test_if_authenticated_user_is_subscribed_to_thread()
    {
        $this->signIn();

        $thread = create('App\Thread');

        $this->assertFalse($thread->isSubscribedTo);

        $thread->createSubscriptionBy(auth()->user()->id);

        $this->assertTrue($thread->isSubscribedTo);
    }

    public function test_a_thread_notifies_all_subscribers_when_a_reply_is_added()
    {
        Notification::fake();

        $this->signIn();

        $thread = create('App\Thread');

        $thread->createSubscriptionBy(auth()->user()->id);

        $thread->addReply([
            'body' => 'Foobar',
            'user_id' => 1
        ]);

        //send notification check here
        Notification::assertSentTo(auth()->user(), ThreadWasUpdated::class);
    }
}
