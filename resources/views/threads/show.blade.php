@extends('layouts.app')

@section('content')
    <thread-view :initial-replies-count="{{ $thread->replies_count }}" inline-template>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="card border-primary mb-3">
                        <div class="card-header">
                            <div class="level">
                                <span class="flex">
                                    <a href="/profiles/{{ $thread->creator->name }}">{{ $thread->creator->name }}</a> posted:
                                    {{ $thread->title }}
                                </span>

                                @can('delete', $thread)
                                    <form action="{{ $thread->path() }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-link" type="submit">Delete thread</button>
                                    </form>
                                @endcan
                            </div>
                        </div>

                        <div class="card-body">
                            {{ $thread->body }}
                        </div>
                    </div>

                    <replies @remove="minusRepliesCount" @add="addRepliesCount"></replies>

                    {{-- {{ $replies->links() }} --}}

                </div>

                <div class="col-md-4">
                    <div class="card mb-3">
                        <div class="card-body">
                            <p>
                                This thread is created {{ $thread->created_at->diffForHumans() }} by
                                <a href="/profiles/{{ $thread->creator->name }}">{{ $thread->creator->name }}</a>, and currently has
                                <span v-text="repliesCount"></span>
                                {{ str_plural('comment', $thread->replies_count) }}
                            </p>
                            <p>
                                <subscribe-button :thread="{{ json_encode($thread) }}"></subscribe-button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </thread-view>
@endsection
