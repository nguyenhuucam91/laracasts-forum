@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="page-header mb-4">
                    <h1>
                        {{ $profileUser->name }}
                        <small>Since: {{ $profileUser->created_at->diffForHumans() }}</small>
                    </h1>
                </div>

                @forelse ($activities as $date => $activity)
                    <h3 class="page-header">{{ $date }}</h3>
                    <hr />
                    @foreach ($activity as $record)
                        @if(view()->exists("profiles.activities.{$record->type}"))
                            @include("profiles.activities.{$record->type}", ['activity' => $record])
                        @endif
                    @endforeach
                @empty
                    <p>There is no activities for this user</p>
                @endforelse
            </div>
        </div>

        {{-- {{ $threads->links() }} --}}
    </div>
@endsection
