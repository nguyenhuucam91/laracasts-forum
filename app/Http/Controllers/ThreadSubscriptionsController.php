<?php

namespace App\Http\Controllers;

use App\Thread;
use Illuminate\Http\Request;

class ThreadSubscriptionsController extends Controller
{
    public function store($channel, Thread $thread)
    {
        return $thread->createSubscriptionBy(auth()->user()->id);
    }

    public function destroy($channel, Thread $thread)
    {
        return $thread->deleteSubscriptionFrom(auth()->user()->id);
    }
}
