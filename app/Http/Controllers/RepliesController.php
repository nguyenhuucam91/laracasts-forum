<?php

namespace App\Http\Controllers;

use App\Http\Requests\Reply\ReplyStoreRequest;
use App\Notifications\MentionUserNotification;
use App\Reply;
use App\Thread;
use App\User;
use Illuminate\Support\Facades\Notification;

class RepliesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($channelId, Thread $thread)
    {
        if (request()->expectsJson()) {
            return $thread->replies()->paginate(10);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store thread content
     *
     * @param [type] $channelId
     * @param Thread $thread
     * @return void
     */
    public function store(ReplyStoreRequest $request, $channelId, Thread $thread)
    {
        $reply = $thread->addReply([
            'body' => $request->input('body'),
            'user_id' => auth()->user()->id
        ]);

        return response()->json(['reply' => $reply->load('owner')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function show(Reply $reply)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function edit(Reply $reply)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function update(Reply $reply)
    {
        $this->authorize('update', $reply);

        $reply->update(request()->input());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reply $reply)
    {
        $this->authorize('delete', $reply);

        $reply->delete();
    }
}
