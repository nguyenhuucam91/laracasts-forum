<?php

namespace App\Http\Controllers;

use App\Thread;
use App\Channel;
use App\Http\Requests\Thread\ThreadStoreRequest;
use Illuminate\Http\Request;

class ThreadsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Channel $channel
     * @param ThreadFilters $filters
     * @return void
     */
    public function index(Channel $channel)
    {
        // Viết 1 scopeFilter() trong App\Thread để filter querystring
        $threads = Thread::filter(request()->query())->latest();

        if ($channel->exists) {
            $threads->where('channel_id', $channel->id);
        }
        //chaining latest, orderBy, where... nếu cần thiết: $threads->where('A', 'B')
        $threads = $threads->get();

        if (request()->wantsJson()) {
            return $threads;
        }

        return view('threads.index', compact('threads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('threads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ThreadStoreRequest $request)
    {
        $thread = Thread::create([
            'user_id' => auth()->id(),
            'title' => $request->input('title'),
            'body' => $request->input('body'),
            'channel_id' => $request->input('channel_id')
        ]);

        return redirect($thread->path())->with('flash', "Your thread has been published");
    }

    /**
     * Display spec for show
     *
     * @param $channelId
     * @param Thread $thread
     * @return void
     */
    public function show($channelId, Thread $thread)
    {
        return view('threads.show', compact('thread'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function edit(Thread $thread)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Thread $thread)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function destroy($channel, Thread $thread)
    {
        $this->authorize('delete', $thread);

        $thread->delete();

        if (request()->wantsJson()) {
            return response()->json([], 204);
        }

        return redirect('/threads');
    }
}
