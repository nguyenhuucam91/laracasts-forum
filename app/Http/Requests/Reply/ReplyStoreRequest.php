<?php

namespace App\Http\Requests\Reply;

use App\Exceptions\ByPassRateLimitingException;
use App\Rules\SpamFree;
use App\Utils\Throttle\ThrottleRequest;
use Illuminate\Foundation\Http\FormRequest;

class ReplyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(ThrottleRequest $throttleRequest)
    {
        return $throttleRequest->check($this, 30, 1);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'body' => ['required', new SpamFree]
        ];
    }

    /**
     * Failed validation
     *
     * @return void
     */
    protected function failedAuthorization()
    {
        throw new ByPassRateLimitingException();
    }
}
