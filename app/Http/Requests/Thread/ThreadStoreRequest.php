<?php

namespace App\Http\Requests\Thread;

use App\Exceptions\ByPassRateLimitingException;
use App\Rules\SpamFree;
use App\Utils\Throttle\ThrottleRequest;
use Illuminate\Foundation\Http\FormRequest;

class ThreadStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(ThrottleRequest $throttleRequest)
    {
        return $throttleRequest->check($this, 60, 1);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', new SpamFree],
            'body' => ['required', new SpamFree],
            'channel_id' => 'required|exists:channels,id'
        ];
    }

    /**
     * Failed authorization exception
     *
     * @return void
     */
    protected function failedAuthorization()
    {
        throw new ByPassRateLimitingException();
    }
}
