<?php

namespace App\Filters;

abstract class BaseFilters
{
    protected $builder;

    // builder được setup từ app()->make() ở trait Filterable
    public function __construct($builder)
    {
        $this->builder = $builder;
    }

    // hàm filter được gọi trong trait, method apply()
    public function filter($params)
    {
        if (count($params) > 0) {
            // loop qua danh sách params dưới dạng field=>value
            foreach ($params as $field => $value) {
                //transform field to camel_case => để viết hàm cho chuẩn PSR
                // kiểm tra xem trong class có hàm $field() không, nếu không có thì bỏ qua lọc $field đó
                if (method_exists($this, camel_case($field))) {
                    // gọi method trong class Filter, truyền vào $value làm param của hàm $field, tức là $field($value)
                    call_user_func_array([$this, camel_case($field)], [$value]);
                }
            }
            // trả lại về $this->builder, cho phép chaining method khác như latest(), orderBy()...
            return $this->builder;
        }
    }
}
