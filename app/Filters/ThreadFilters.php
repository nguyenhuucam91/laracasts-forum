<?php

namespace App\Filters;

use App\User;

// extends từ BaseFilters class để sử dụng phần __construct() và filter() trong parent class
class ThreadFilters extends BaseFilters
{
    // xử lí với /threads?by=
    public function by($name)
    {
        $user = User::where('name', $name)->firstOrFail();
        return $this->builder->where('user_id', $user->id);
    }

    /**
     * Sort by popularity, xử lí với /threads?popular
     */
    public function popular()
    {
        return $this->builder->orderBy('replies_count', 'desc');
    }

    /**
     * Sort by popularity, xử lí với /threads?unanswered
     */
    public function unanswered()
    {
        return $this->builder->has('replies', '=', 0);
    }
}
