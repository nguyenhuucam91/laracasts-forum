<?php

namespace App\Traits;

use Exception;

trait Filterable
{
    // Viết scope ở đây để không cần viết trong Model nữa, giảm lặp code filter trong Model.
    // $query: return Illuminate\Database\Eloquent\Builder
    // $params: request()->query() đã được truyền vào thông qua controller action.

    public function scopeFilter($query, $params)
    {
        return $this->apply($query, $params);
    }

    // tách hàm apply riêng cho method "sáng", dễ nhìn
    public function apply($query, $params)
    {
        if (!property_exists($this, 'filterable')) {
            throw new Exception("Using Filterable trait requires \$filterable property in your " . get_class($this));
        }
        // tạo 1 instance của $this->filterable: attribute của class App\Thread model,
        // -> truyền query builder $query làm params trong __construct của $this->filterable
        $filterable = app()->make($this->filterable, ['builder' => $query]);
        // gọi hàm filter khi được khởi tạo 1 instance của class $this->filterable
        return $filterable->filter($params);
    }
}
