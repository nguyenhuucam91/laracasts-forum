<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Response;

class ByPassRateLimitingException extends Exception
{
    protected $message = "You are posting too many times. Take a break";
    protected $code = 429;
}
