<?php

namespace App\Listeners;

use App\Events\NotifyUser;
use App\Notifications\ThreadWasUpdated;

class NotifySubscribers
{
    /**
     * Handle the event.
     *
     * @param  NotifyUser  $event
     * @return void
     */
    public function handle(NotifyUser $event)
    {
        $reply = $event->reply;
        return $reply->thread->subscriptions->where('user_id', '!=', $reply->user_id)
            ->each(function ($subscription) use ($reply) {
                $subscription->user->notify(new ThreadWasUpdated($reply));
            });
    }
}
