<?php

namespace App\Listeners;

use App\Notifications\MentionUserNotification;
use App\User;

class MentionUser
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $reply = $event->reply;
        if (preg_match_all('/(?<=@)[\w\-]+/', $reply->body, $matches)) {
            $users = User::whereIn('name', $matches[0])->get();
            foreach ($users as $user) {
                $user->notify(new MentionUserNotification($reply));
            }
        }
    }
}
