<?php

namespace App\Utils\Throttle;

use GrahamCampbell\Throttle\Facades\Throttle;

class ThrottleRequest
{
    /**
     * Check request rate within limit or not
     *
     * @param [type] $request
     * @param int $requestAttempts: Number of attempts to make a request
     * @param int $timeWithin: Time allowed within request attempts, calculated in minute
     * @return void
     */
    public function check($request, $requestAttempts, $timeWithin)
    {
        $throttle = Throttle::get($request, $requestAttempts, $timeWithin);
        $isRequestWithinThrottleLimit = $throttle->attempt($request);

        return $isRequestWithinThrottleLimit;
    }
}
