<?php

namespace App\Utils\Inspections;

class Spam
{
    private $detections = [
        InvalidKeywords::class,
        KeyHeldDown::class
    ];

    public function spamDetected($keyword)
    {
        foreach ($this->detections as $detection) {
            $detection = app()->make($detection);
            $isKeywordSpammed = $detection->isSpammed($keyword);
            if ($isKeywordSpammed) {
                return true;
            }
        }

        return false;
    }
}
