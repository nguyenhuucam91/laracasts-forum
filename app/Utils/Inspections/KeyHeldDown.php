<?php

namespace App\Utils\Inspections;

class KeyHeldDown
{
    /**
     * Check if number of consecutive chars is greater than 4 or not
     *
     * @param [type] $keyword
     * @return void
     */
    public function isSpammed($keyword)
    {
        if (preg_match('/(.)\\1{4,}/', $keyword)) {
            return true;
        }

        return false;
    }
}
