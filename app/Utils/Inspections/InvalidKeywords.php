<?php

namespace App\Utils\Inspections;

class InvalidKeywords
{
    /**
     * Listing available spam keywords
     *
     * @return void
     */
    public function availableSpamKeywords(): array
    {
        return [
            'Yahoo customer support'
        ];
    }

    /**
     * Detect keyword inside spam keywords or not
     *
     * @param [type] $keyword
     * @return void
     */
    public function isSpammed($keyword)
    {
        $getSpamKeywords = $this->availableSpamKeywords();

        foreach ($getSpamKeywords as $spamKeyword) {
            if (strtolower($keyword) == strtolower($spamKeyword)) {
                return true;
            }
        }

        return false;
    }
}
