<?php

namespace App;

use App\Events\NotifyUser;
use App\Filters\ThreadFilters;
use App\Traits\Filterable;
use App\Traits\RecordsActivity;
use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    use Filterable, RecordsActivity;

    protected $guarded = [];

    protected $with = ['creator', 'channel'];

    protected $appends = ['isSubscribedTo'];

    protected $filterable = ThreadFilters::class;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('replies_count', function ($builder) {
            $builder->withCount('replies');
        });

        static::deleting(function ($thread) {
            $thread->replies->each(function ($reply) {
                $reply->delete();
            });
        });
    }

    public function path()
    {
        return "/threads/{$this->channel->slug}/{$this->id}";
    }

    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    /**
     * A thread belongs to user
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Add a new reply
     */
    public function addReply($reply)
    {
        $reply = $this->replies()->create($reply);

        event(new NotifyUser($reply));

        return $reply;
    }
    /**
     * A thread belongs to a channel
     */
    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    public function createSubscriptionBy($userId = null)
    {
        return $this->subscriptions()->create([
            'user_id' => $userId ?: auth()->id()
        ]);
    }

    public function deleteSubscriptionFrom($userId = null)
    {
        return $this->subscriptions()->where([
            'user_id' => $userId ?: auth()->id()
        ])->delete();
    }

    public function subscriptions()
    {
        return $this->hasMany(ThreadSubscription::class);
    }

    public function getIsSubscribedToAttribute()
    {
        return $this->subscriptions()->where(['user_id' => auth()->id()])->exists();
    }
}
